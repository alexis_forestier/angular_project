import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing-page/components/landing-page.component';
import { ObservableComponent } from './observable/observable.component';
import { ObservableTrainComponent } from './observable-train/observable-train.component';

const routes: Routes = [
  { path: 'images', loadChildren: () => import('./images/images.module').then(m => m.ImagesModule)},
  { path: 'observable', component: ObservableComponent },
  { path: 'train_obs', component: ObservableTrainComponent },
  { path: '', component: LandingPageComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
